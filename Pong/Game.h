//
//  Game.h
//  Pong
//
//  Created by Emilie Myhrman on 2016-02-21.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import <UIKit/UIKit.h>

int X;
int Y;
int playerScore;
int npcScore;
int remainingCounts;

@interface Game : UIViewController

{
    NSTimer *animationTimer;
    NSTimer *countdownTimer;
    NSString *resultText;

}

- (void)BallAnimation;


@end
