//
//  Game.m
//  Pong
//
//  Created by Emilie Myhrman on 2016-02-21.
//  Copyright © 2016 Emilie Myhrman. All rights reserved.
//

#import "Game.h"

@interface Game ()
@property (weak, nonatomic) IBOutlet UIImageView *pongBall;
@property (weak, nonatomic) IBOutlet UIImageView *playerBoard;
@property (weak, nonatomic) IBOutlet UIImageView *npcBoard;
@property (weak, nonatomic) IBOutlet UILabel *npcScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *countdownLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultMessage;
@property (weak, nonatomic) IBOutlet UIButton *quitButton;

@end

@implementation Game

- (void)SetCountdown {
    remainingCounts = 4;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(StartCountdown) userInfo:nil repeats:YES];

}

- (void)StartCountdown {
    _countdownLabel.hidden = NO;
    remainingCounts = remainingCounts - 1;
    
    NSString *countdownDigit = [NSString stringWithFormat:@"%i", remainingCounts];
    _countdownLabel.text = countdownDigit;
        
    if (remainingCounts == 0) {
        [countdownTimer invalidate];
        countdownTimer = nil;
        _countdownLabel.hidden = YES;
        [self GameStart];
            
    }

}

- (void)GameStart {
    _quitButton.hidden = YES;
    //50% chance of ball moving in either right or left direction.
    //ball cannot move horizontal left or right, must be moving in a vertical direction as well.
    
    X = arc4random() %15;
    X = X - 7;
    
    Y = arc4random() %15;
    Y = Y - 7;
    
    if (X == 0) {
        X = 1;
    }
    
    if (Y == 0) {
        Y = 1;
    }
    
    //makes everything move on screen every 0,01 second
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(BallAnimation) userInfo:nil repeats:YES];

}


- (void)BallAnimation {
    
    [self NpcAI];
    [self BallCollision];
    
    _pongBall.center = CGPointMake(_pongBall.center.x + X, _pongBall.center.y + Y);
    
    if (_pongBall.center.x < 10) {
        X = 0 - X;
    }
    
    if (_pongBall.center.x > 310) {
        X = 0 - X;
    }
    
    if (_pongBall.center.y < 0) {
        playerScore ++;
        _playerScoreLabel.text = [NSString stringWithFormat:@"%i", playerScore];
        [animationTimer invalidate];
        
        //display win label
        _resultMessage.hidden = NO;
        resultText = @"Win! You get +1 score.";
        _resultMessage.text = resultText;
        
        _quitButton.hidden = NO;
        
        //start countdown for next game
        [self SetCountdown];
        
        _pongBall.center = CGPointMake(150, 274);
        
    }
    
    if (_pongBall.center.y > 550) {
        npcScore ++;
        _npcScoreLabel.text = [NSString stringWithFormat:@"%i", npcScore];
        [animationTimer invalidate];
        
        //display lost label
        _resultMessage.hidden = NO;
        resultText = @"Loss! NPC get +1 score.";
        _resultMessage.text = resultText;
        
        _quitButton.hidden = NO;
        
        //start countdown for next game
        [self SetCountdown];
        
    }
    

}


- (void)NpcAI {
    
    //makes the npc follow the ball's direction
    if (_npcBoard.center.x > _pongBall.center.x) {
        _npcBoard.center = CGPointMake(_npcBoard.center.x - 1, _npcBoard.center.y);
    }
    
    if (_npcBoard.center.x < _pongBall.center.x) {
        _npcBoard.center = CGPointMake(_npcBoard.center.x + 1, _npcBoard.center.y);
    }
    
    
    //dont let the npc move outside of the screen (4-inch values)
    if (_npcBoard.center.x < 45) {
        _npcBoard.center = CGPointMake(45, _npcBoard.center.y);
    }
    
    if (_npcBoard.center.x > 275) {
        _npcBoard.center = CGPointMake(275, _npcBoard.center.y);
    }


}


- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch *playerTouch = [[event allTouches] anyObject];
    _playerBoard.center = [playerTouch locationInView:self.view];
    
    if (_playerBoard.center.y > 538) {
        _playerBoard.center = CGPointMake(_playerBoard.center.x, 538);
    }
    
    if (_playerBoard.center.y < 538) {
        _playerBoard.center = CGPointMake(_playerBoard.center.x, 538);
    }
    
    if (_playerBoard.center.x < 45) {
        _playerBoard.center = CGPointMake(45, _playerBoard.center.y);
    }
    
    if (_playerBoard.center.x > 275) {
        _playerBoard.center = CGPointMake(275, _playerBoard.center.y);
    }

}


- (void)BallCollision {
    
    if (CGRectIntersectsRect(_pongBall.frame, _playerBoard.frame)) {
        Y = arc4random() %6;
        Y = 0 - Y;
    }
    
    if (CGRectIntersectsRect(_pongBall.frame, _npcBoard.frame)) {
        Y = arc4random() %6;
        Y = 0 + Y;
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self SetCountdown];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
